import { Component, OnInit, ViewChild } from '@angular/core';
import { Role } from 'src/app/models/role';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-role',
  templateUrl: './list-role.component.html',
  styleUrls: ['./list-role.component.scss']
})
export class ListRoleComponent implements OnInit {
  Roles: Role[];
  displayedColumns: string[] = ['id', 'nombre', 'actions'];
  dataSource: MatTableDataSource<Role>;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private dataApi: UserService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
    this.getListRoles();
  }

  getListRoles(): void {
    this.dataApi.getAllRoles().subscribe(
      roles => { 
        this.dataSource = new MatTableDataSource(roles);
        this.dataSource.paginator = this.paginator;
      }
    );
  }
}
