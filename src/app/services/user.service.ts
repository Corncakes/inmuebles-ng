import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Router } from '@angular/router';
import { Role } from '../models/role';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrlApi = 'https://ipcc-core.herokuapp.com/';  // URL to web api

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private router: Router,
    private http: HttpClient
  ) { }

  getAllRoles(): Observable<Role[]> {
    return this.http.get<Role[]>(this.baseUrlApi + 'rol/');
  }
}
